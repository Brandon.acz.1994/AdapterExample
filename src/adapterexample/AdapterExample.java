/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapterexample;

/**
 *
 * @author Brandon
 */
public class AdapterExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    Automovil Tsuru = new Automovil(new MotorComun());   
   
    Tsuru.Encender();
    Tsuru.Apagar();
    Tsuru.Acelerar();
    System.out.println();
    
    Tsuru.setMotor(new MotorEconomico());
    
    Tsuru.Encender();
    Tsuru.Apagar();
    Tsuru.Acelerar();
    System.out.println();
    
    Tsuru.setMotor(new AdaptadorElectricoToGasolina(new MotorElectrico()));
    
    Tsuru.Encender();
    Tsuru.Acelerar();
    Tsuru.Apagar();
    System.out.println();    
    
}
    
    
        
    
    
}
