/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapterexample;

/**
 *
 * @author Brandon
 */
public interface MotorElectricidad {
    
    
    public void Conectar();
    public void Activar();
    public void moverMasRapido();
    public void Detener();
    public void Desconectar();
    
}
