/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapterexample;

/**
 *
 * @author Brandon
 */
public class AdaptadorElectricoToGasolina implements MotorGasolina {

     private final MotorElectrico motorElectrico;
 
    public AdaptadorElectricoToGasolina(MotorElectrico motor){
        super();
        this.motorElectrico = motor;
        System.out.println("Creando motor Electrico adapter");
    }
    @Override
    public void Encender() {
        System.out.println("Encendiendo motorElectricoAdapter");
        this.motorElectrico.Conectar();
        this.motorElectrico.Activar();
    }
 
    @Override
    public void Acelerar() {
        System.out.println("Acelerando motor electrico...");
        this.motorElectrico.moverMasRapido();
    }
 
    @Override
    public void Apagar() {
        System.out.println("Apagando motor electrico");
        this.motorElectrico.Detener();
        this.motorElectrico.Desconectar();
    }
    
}
