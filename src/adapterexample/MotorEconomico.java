/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapterexample;

/**
 *
 * @author Brandon
 */
public class MotorEconomico implements MotorGasolina {
    
    public MotorEconomico(){
        super();
        System.out.println("Creando el motor Economico");
    }
 
    @Override
    public void Encender() {
        System.out.println("encendiendo motor comun");
    }
 
    @Override
    public void Acelerar() {
        System.out.println("acelerando el motor comun");
    }
 
    @Override
    public void Apagar() {
        System.out.println("Apagando motor comun");
    }
}
