/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapterexample;

/**
 *
 * @author Brandon
 */
public class Automovil extends AutomovilAbstracto  {
   private  MotorGasolina motor;
   
    Automovil(MotorGasolina motor){ 
         this.motor= motor;         
    }
    
   @Override
    public void Encender(){
    motor.Encender();
    }
    
   @Override
    public void Apagar(){
    motor.Apagar();
    }
    
   @Override
    public void Acelerar(){
    motor.Acelerar();
    }
    
   @Override
    public void setMotor(MotorGasolina motor){
    this.motor=motor;
    }
    
}
